<?php

namespace Back\Back\Models;

use Phalcon\Mvc\Model as Modelo;

class ApiModel extends Modelo
{

    public function holaMundo()
    {
        $response = null;
        
        $di = \Phalcon\DI::getDefault();
        $db = $di->get('conexion');
        $statement = $db->prepare(
            "SELECT 'hola mundo!' AS saludo, now() AS actual;"
        );
        $statement->execute();
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            $resultSet = new \stdClass();
            $resultSet->saludo = $entry["saludo"];
            $resultSet->actual = $entry["actual"];
            $response = $resultSet;
            $resultSet = null;
        }

        return $response;
    }

    public function grabaPerfilModel($usuario)
    {
        $response = null;
        
        $di = \Phalcon\DI::getDefault();
        $db = $di->get('db_sql');
        $statement = $db->prepare("exec grabaPerfil :_usuario;");
            $statement->bindValue('_usuario',$usuario,\PDO::PARAM_STR);

            //$statement2->bindValue('numemp',$rowSet->numemp,\PDO::PARAM_INT);
            //$statement2->bindValue('des_empleado',$rowSet->des_empleado,\PDO::PARAM_STR);
        $statement->execute();
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            
            $resultSet = new \stdClass();
            $resultSet->id_usuario = $entry["id_usuario"];
            $resultSet->usuario = $entry["usuario"];
            $response = $resultSet;
            $resultSet = null;
        }

        return $response;
    }

     public function grabaPrestamoModel($id_usuario, $monto, $interes, $deuda, $edad, $tarjeta)
    {
        $response = null;
        
        $di = \Phalcon\DI::getDefault();
        $db = $di->get('db_sql');
        $statement = $db->prepare("exec grabaPrestamo :_usuario, :_monto, :_interes, :_deuda, :_edad, :_tarjeta  ;");
            $statement->bindValue('_usuario',$id_usuario,\PDO::PARAM_INT);
            $statement->bindValue('_monto',$monto,\PDO::PARAM_INT);
            $statement->bindValue('_interes',$interes,\PDO::PARAM_INT);
            $statement->bindValue('_deuda',$deuda,\PDO::PARAM_INT);
            $statement->bindValue('_edad',$edad,\PDO::PARAM_INT);
            $statement->bindValue('_tarjeta',$tarjeta,\PDO::PARAM_INT);
        
        $statement->execute();
        $resultSet = new \stdClass();
        $resultSet->estado = 1;
        $response = $resultSet;
        $resultSet = null;
       
        return $response;
    }

    public function consultaHistoriaModel($id_usuario)
    {
        $response = null;
        
        $di = \Phalcon\DI::getDefault();
        $db = $di->get('db_sql');
        $statement = $db->prepare("exec consultarHistorial :_id_usuario;");
            $statement->bindValue('_id_usuario',$id_usuario,\PDO::PARAM_INT);            
        $statement->execute();
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            
            $resultSet = new \stdClass();
            $resultSet->folio = $entry["folio"];
            $resultSet->id_usuario = $entry["id_usuario"];
            if($entry["estatus"]==0){
                $resultSet->estatus ='Pendiente';
            }else if($entry["estatus"]==1){
                $resultSet->estatus ='Aceptada';
            }
            else if($entry["estatus"]==2){
                $resultSet->estatus ='Cancelada';
            }
            $resultSet->monto_prestamo = $entry["monto_prestamo"];
            $resultSet->interes = $entry["interes"];
            $resultSet->deuda = $entry["deuda"];
            $resultSet->fecha = $entry["fecha"];
            $response[] = $resultSet;
            $resultSet = null;
        }
        return $response;
    }

    public function consultaHistoriaPendientesModel($id_usuario)
    {
        $response = null;
        
        $di = \Phalcon\DI::getDefault();
        $db = $di->get('db_sql');
        $statement = $db->prepare("exec consultarHistorialPendientes :_id_usuario;");
            $statement->bindValue('_id_usuario',$id_usuario,\PDO::PARAM_INT);            
        $statement->execute();
        while ($entry = $statement->fetch(\PDO::FETCH_ASSOC)) {
            
            $resultSet = new \stdClass();
            $resultSet->folio = $entry["folio"];
            $resultSet->id_usuario = $entry["id_usuario"];
            if($entry["estatus"]==0){
                $resultSet->estatus ='Pendiente';
            }else if($entry["estatus"]==1){
                $resultSet->estatus ='Aceptada';
            }
            else if($entry["estatus"]==2){
                $resultSet->estatus ='Cancelada';
            }           
            $resultSet->monto_prestamo = $entry["monto_prestamo"];
            $resultSet->interes = $entry["interes"];
            $resultSet->deuda = $entry["deuda"];
            $resultSet->fecha = $entry["fecha"];
            $response[] = $resultSet;
            $resultSet = null;
        }
        return $response;
    }

   
}
