<?php

use Coppel\RAC\Modules\IModule;
use Phalcon\Mvc\Micro\Collection;
use Katzgrau\KLogger\Logger;

class Module implements IModule
{

    public function __construct()
    {
    }

    public function registerLoader($loader)
    {
        $loader->registerNamespaces(array('Back\Back\Controllers' => __DIR__.'/controllers/',
        'Back\Back\Models' => __DIR__.'/models/'
        ), true);
    }

    public function getCollections()
    {
        $collection = new Collection();

        $collection->setPrefix('/api')
        ->setHandler('\Back\Back\Controllers\ApiController')
        ->setLazy(true);

        $collection->get('/ejemplo', 'holaMundo');
        $collection->get('/grabaperfil/{usuario}', 'grabaPerfilController');
        $collection->get('/grabaprestamo/{id_usuario}/{monto}/{interes}/{deuda}/{edad}/{tarjeta}', 'grabaPrestamoController');
        $collection->get('/consultahistorial/{id_usuario}', 'consultaHistorialController');
        $collection->get('/consultahistorialpendientes/{id_usuario}', 'consultaHistorialPendientesController');
      

        return [$collection];
    }

    public function registerServices()
    {
        $di = Phalcon\DI::getDefault();
        $di->set('conexion', function () use ($di) {
            $config = $di->get('config');
            $host = $config->db->host;
            $dbname = $config->db->dbname;
            return new \PDO(
                "mysql:host=$host;dbname=$dbname",
                $config->db->username,
                $config->db->password,
                array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
            );
        });

        $di->set('db_sql', function() use ($di) 
        {
            $config = $di->get('config');
            $host = $config->db_sql->host;
            $dbname = $config->db_sql->dbname;
            return new \PDO("sqlsrv:server=$host;Database=$dbname",
                //return new \PDO("dblib:host=$host;dbname=$dbname;charset=utf8",
                $config->db_sql->username,
                $config->db_sql->password,
                array(\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION)
            );
        });  

        $di->set('logger', function () {
            return new Logger('logs');
        });
    }
}
