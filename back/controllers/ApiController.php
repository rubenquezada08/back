<?php

namespace Back\Back\Controllers;

use Coppel\RAC\Controllers\RESTController;
use Coppel\RAC\Exceptions\HTTPException;
use Back\Back\Models as Modelos;

class ApiController extends RESTController
{

    private $logger;
    private $modelo;

    public function onConstruct()
    {
        $this->logger = \Phalcon\DI::getDefault()->get('logger');
        $this->modelo = new Modelos\ApiModel();
    }

    public function holaMundo()
    {
        $response = null;
        try {
            $response = $this->modelo->holaMundo();
        } catch (\Exception $ex) {
            $mensaje = utf8_encode($ex->getMessage());
            $this->logger->error(
                "[".__METHOD__ ."]"."Se lanzó la excepción ".$mensaje
            );
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500,
                array(
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                )
            );
        }
        return $this->respond(["response" => $response]);
    }
    public function grabaPerfilController($usuario)
    {
        $response = null;
        try {
            $response = $this->modelo->grabaPerfilModel($usuario);
        } catch (\Exception $ex) {
            $mensaje = utf8_encode($ex->getMessage());
            $this->logger->error(
                "[".__METHOD__ ."]"."Se lanzó la excepción ".$mensaje
            );
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500,
                array(
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                )
            );
        }
        return $this->respond(["response" => $response]);
    }
    public function grabaPrestamoController($id_usuario, $monto, $interes, $deuda, $edad, $tarjeta)
    {
        $response = null;
        try {
            $response = $this->modelo->grabaPrestamoModel($id_usuario, $monto, $interes, $deuda, $edad, $tarjeta);
        } catch (\Exception $ex) {
            $mensaje = utf8_encode($ex->getMessage());
            $this->logger->error(
                "[".__METHOD__ ."]"."Se lanzó la excepción ".$mensaje
            );
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500,
                array(
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                )
            );
        }
        return $this->respond(["response" => $response]);
    }

    public function consultaHistorialController($id_usuario)
    {
        $response = null;
        try {
            $response = $this->modelo->consultaHistoriaModel($id_usuario);
        } catch (\Exception $ex) {
            $mensaje = utf8_encode($ex->getMessage());
            $this->logger->error(
                "[".__METHOD__ ."]"."Se lanzó la excepción ".$mensaje
            );
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500,
                array(
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                )
            );
        }
        return $this->respond(["response" => $response]);
    }

    public function consultaHistorialPendientesController($id_usuario)
    {
        $response = null;
        try {
            $response = $this->modelo->consultaHistoriaPendientesModel($id_usuario);
        } catch (\Exception $ex) {
            $mensaje = utf8_encode($ex->getMessage());
            $this->logger->error(
                "[".__METHOD__ ."]"."Se lanzó la excepción ".$mensaje
            );
            throw new HTTPException(
                'No fue posible completar su solicitud, intente de nuevo por favor.',
                500,
                array(
                    'dev' => $mensaje,
                    'internalCode' => 'SIE1000',
                    'more' => 'Verificar conexión con la base de datos.'
                )
            );
        }
        return $this->respond(["response" => $response]);
    }
    
}
